package com.armirene.testapiarmirene;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApiArmireneApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApiArmireneApplication.class, args);
	}

}
