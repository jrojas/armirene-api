package com.armirene.testapiarmirene.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.armirene.testapiarmirene.entities.User;

/**
 * Dao User
 * 
 * @author Usuario
 *
 */
public interface IUserRepository extends JpaRepository<User, Long> {

}
