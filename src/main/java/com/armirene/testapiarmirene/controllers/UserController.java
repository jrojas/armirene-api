package com.armirene.testapiarmirene.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.armirene.testapiarmirene.dtos.UserResponse;
import com.armirene.testapiarmirene.entities.User;
import com.armirene.testapiarmirene.services.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Api User Controller
 * 
 * @author Usuario
 *
 */
@RestController
@RequestMapping(value = "api/users")
public class UserController {

	@Autowired
	IUserService userService;

	@Autowired
	ModelMapper modelMapper;

	/**
	 * Service get all users
	 * 
	 * @return
	 */
	@GetMapping("")
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ApiOperation(value = "Get all users", notes = "Get all users.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succes"),
			@ApiResponse(code = 500, message = "Internal server error processing the operation.")

	})
	public ResponseEntity<List<UserResponse>> getUsers() {
		List<User> userList = userService.findAll();
		List<UserResponse> userListResponse = userList.stream().map(user -> modelMapper.map(user, UserResponse.class))
				.collect(Collectors.toList());
		return ResponseEntity.ok(userListResponse);
	}

}
