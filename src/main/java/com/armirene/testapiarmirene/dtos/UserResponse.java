package com.armirene.testapiarmirene.dtos;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model Response User
 * 
 * @author Usuario
 *
 */
@ApiModel(description = "User response")
public class UserResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Id  user", example = "123456")
	private Long id;

	@ApiModelProperty(notes = "Name user", example = "Juan Roa")
	private String name;

	@ApiModelProperty(notes = "Email user", example = "juan.roja@gmail.com")
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
