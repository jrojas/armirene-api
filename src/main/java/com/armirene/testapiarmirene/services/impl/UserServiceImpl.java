package com.armirene.testapiarmirene.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.armirene.testapiarmirene.entities.User;
import com.armirene.testapiarmirene.repositories.IUserRepository;
import com.armirene.testapiarmirene.services.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserRepository userRepository;

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

}
