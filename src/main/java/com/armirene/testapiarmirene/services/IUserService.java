package com.armirene.testapiarmirene.services;

import java.util.List;

import com.armirene.testapiarmirene.entities.User;

public interface IUserService {

	/**
	 * Service to find all user in database
	 * 
	 * @return
	 */
	public List<User> findAll();

}
